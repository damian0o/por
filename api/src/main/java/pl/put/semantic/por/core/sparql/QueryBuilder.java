package pl.put.semantic.por.core.sparql;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;

public class QueryBuilder {
	
	public static QueryExecution execQuery(final String sparqlEndpoint, final String queryString) {
		LOGGER.trace("Execute query {} against endpoint {}",queryString,sparqlEndpoint);
		
		final Query query = QueryFactory.create(queryString);
		final QueryExecution qexec = QueryExecutionFactory.sparqlService(sparqlEndpoint, query);
		
		LOGGER.trace("Execute query result {}",qexec.getDataset());
		return qexec;
	}
	
	private static final Logger LOGGER = LoggerFactory.getLogger(QueryBuilder.class);
	
	public static void main(String[] args) {
		
	}
}
