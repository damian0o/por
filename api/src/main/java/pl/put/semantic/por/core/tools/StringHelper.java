package pl.put.semantic.por.core.tools;

public class StringHelper {
	
	public static final String COMMA = ",";

	public static final String EMPTY_STRING = "";

	public static final String XSD_STRING = "^^http://www.w3.org/2001/XMLSchema#string";

	public static final String ESCAPED_QUOTE = "\\\"";

	public static final String QUOTE = "\"";

}
